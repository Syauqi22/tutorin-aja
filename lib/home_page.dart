import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:tutorinhoho/register_page.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final GoogleSignIn _googleSignIn = GoogleSignIn();

  bool isLoading = false;

  Future<Null> handleSignOut()async{

    setState(() {
      isLoading = true;
    });
    await FirebaseAuth.instance.signOut();
    await _googleSignIn.disconnect();
    await _googleSignIn.signOut();

    setState((){
      isLoading = false;
    });
    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context)=> RegisterPage()),
    (Route<dynamic> route)=> false);

  }  

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text('wakwaw')
        ),
        body: Center(
          child: RaisedButton(
            onPressed: handleSignOut,
            child: Text("Sign Out"),
          )
        ),
      ),
    );
  }
}
