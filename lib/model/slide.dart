import 'package:flutter/material.dart';

class Slide{
  final String imageUrl;
  final String title;
  final String description;

  Slide({
    @required this.imageUrl,
    @required this.title, 
    @required this.description
    });
}

final slideList = [
  Slide(
    imageUrl: 'assets/image/guru.jpg',
    title: 'Mantap Boy',
    description: 'Kami datang kepada anda dengan guru guru yang berkualitas yang bisa membuat anda menjadi semakin cerdas'
  ),
  Slide(
    imageUrl: 'assets/image/ABDUL.png',
    title: 'Mantap Boy',
    description: 'Kami datang kepada anda dengan guru guru yang berkualitas yang bisa membuat anda menjadi semakin cerdas'
  ),
  Slide(
    imageUrl: 'assets/image/kabah.jpg',
    title: 'Mantap Boy',
    description: 'Kami datang kepada anda dengan guru guru yang berkualitas yang bisa membuat anda menjadi semakin cerdas'
  ),
];